INSERT INTO `user` (`username`, `role`, `password`) VALUES ('admin', 'ADMIN', '$2a$10$lTGqkdHl5XNVUS17WzywDO5ianWAsBoBxCKY.UKd4HEGRl7jUyMKO');
INSERT INTO `user` (`username`, `role`, `password`) VALUES ('user', 'USER', '$2a$10$lTGqkdHl5XNVUS17WzywDO5ianWAsBoBxCKY.UKd4HEGRl7jUyMKO');
INSERT INTO `user` (`username`, `role`, `password`) VALUES ('anonymous', NULL, NULL);