package dev.brewka.example.spring.security.service;

import dev.brewka.example.spring.security.database.User;
import dev.brewka.example.spring.security.database.UserRepo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomerUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (Strings.isNotBlank(username)) {
            User user = userRepo.findByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException(username));
            return new CustomUserDetails(user);
        }

        throw new UsernameNotFoundException("blank username");
    }
}
