package dev.brewka.example.spring.security.controller;

import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @Secured("public")
    @GetMapping
    public String hello(@RequestParam String name) {
        if (Strings.isNotBlank(name)) {
            return "Hello, " + name.trim() + "!";
        } else {
            return "Hello, World!";
        }
    }
}
