package dev.brewka.example.spring.security.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class HomeController {

    @Secured("USER")
    @GetMapping("/home")
    public String home(Authentication auth) {
        log.info("GET /home - User: {} with Auths: {}", auth.getName(), auth.getAuthorities());
        return "This is Home Page";
    }

    @Secured("ADMIN")
    @GetMapping("/admin")
    public String admin(Authentication auth) {
        log.info("GET /admin - User: {} with Auths: {}", auth.getName(), auth.getAuthorities());
        return "This is Admin Page";
    }
}
